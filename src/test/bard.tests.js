var assert = require("assert");
var bardFactory = require("../public/javascripts/Bard.js");

describe('Bard', function() {
	var instance = bardFactory.createBard();
	
	describe('#getAttackBonus(level)', function() {
		it('should return [1] when the value is 1', function() {
			var result = instance.getAttackBonus(1);
			assert.deepEqual([0], result);
		}),
		
		it('should return [5] when the value is 7', function() {
			var result = instance.getAttackBonus(7);
			assert.deepEqual([5], result);
		}),
		
		it('should return [6, 1] when the value is 8', function() {
			var result = instance.getAttackBonus(8);
			assert.deepEqual([6, 1], result);
		}),
		
		it('should return [15, 10, 5] when the value is 20', function() {
			var result = instance.getAttackBonus(20);
			assert.deepEqual([15, 10, 5], result);
		})
	}),
	
	describe('#getSaves(level)', function() {
		it('should return [0, 2, 2] when the value is 1', function() {
			var result = instance.getSaves(1);
			assert.deepEqual([0, 2, 2], result);
		}),
		
		it('should return [0, 3, 3] when the value is 2', function() {
			var result = instance.getSaves(2);
			assert.deepEqual([0, 3, 3], result);
		}),
		
		it('should return [3, 7, 7] when the value is 10', function() {
			var result = instance.getSaves(10);
			assert.deepEqual([3, 7, 7], result);
		}),
		
		it('should return [6, 12, 12] when the value is 20', function() {
			var result = instance.getSaves(20);
			assert.deepEqual([6, 12, 12], result);
		})
	})
})