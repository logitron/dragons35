var assert = require("assert");
var barbarianFactory = require("../public/javascripts/Barbarian.js");

describe('Barbarian', function() {
	var instance = barbarianFactory.createBarbarian();
	
	describe('#getAttackBonus(level)', function() {
		it('should return [1] when the value is 1', function() {
			var result = instance.getAttackBonus(1);
			assert.deepEqual([1], result);
		})
		
		it('should return [5] when the value is 5', function() {
			var result = instance.getAttackBonus(5);
			assert.deepEqual([5], result);
		})
		
		it('should return [6, 1] when the value is 6', function() {
			var result = instance.getAttackBonus(6);
			assert.deepEqual([6, 1], result);
		})
		
		it('should return [19, 14, 9, 4] when the value is 19', function() {
			var result = instance.getAttackBonus(19);
			assert.deepEqual([19, 14, 9, 4], result);
		})
		
		it('should return [20, 15, 10, 5] when the value is 20', function() {
			var result = instance.getAttackBonus(20);
			assert.deepEqual([20, 15, 10, 5], result);
		})
	}),
	
	describe('#getSaves(level)', function() {
		it('should return [2, 0, 0] when the value is 1', function() {
			var result = instance.getSaves(1);
			assert.deepEqual([2, 0, 0], result);
		})
		
		it('should return [3, 0, 0] when the value is 2', function() {
			var result = instance.getSaves(2);
			assert.deepEqual([3, 0, 0], result);
		})
		
		it('should return [5, 2, 2] when the value is 6', function() {
			var result = instance.getSaves(6);
			assert.deepEqual([5, 2, 2], result);
		})
		
		it('should return [11, 6, 6] when the value is 19', function() {
			var result = instance.getSaves(19);
			assert.deepEqual([11, 6, 6], result);
		})
		
		it('should return [12, 6, 6] when the value is 20', function() {
			var result = instance.getSaves(20);
			assert.deepEqual([12, 6, 6], result);
		})
	})
})