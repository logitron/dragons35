var alignmentList = require('../collections/Alignments.json');

exports.list = function(req, res) {
  res.send(alignmentList);
};