
/*
 * GET home page.
 */
exports.index = function(req, res) {
	res.render('index', { title: 'D&D 3.5 Character Generator' });
};