// Creating DAO for Barbarian Class
var Barbarian = function() {
	this.minimumAge = {
		human: 16,
		dwarf: 43,
		elf: 114,
		gnome: 44,
		halfElf: 21,
		halfOrc: 15,
		halfling: 22
	}
}

Barbarian.prototype.getFeatures = function() {
	
}

Barbarian.prototype.getAttackBonus = function(level) {
	var result = [];
	
	var attacks = Math.ceil(level / 5);
	
	for (var i = 0; i < attacks; i++) {
		var offset = i * 5;
		result.push(level - offset);
	}
	
	return result;
}

Barbarian.prototype.getMinimumAge = function(raceId) {
	var result = 0;
	
	switch(raceId) {
		case 0:
			result = this.minimumAge.human;
			break;
		case 1:
			result = this.minimumAge.dwarf;
			break;
		case 2:
			result = this.minimumAge.elf;
			break;
		case 3:
			result = this.minimumAge.gnome;
			break;
		case 4:
			result = this.minimumAge.halfElf;
			break;
		case 5:
			result = this.minimumAge.halfOrc;
			break;
		case 6:
			result = this.minimumAge.halfling;
			break;
		default:
			break;
	}
	
	return result;
}

Barbarian.prototype.getSaves = function(level) {
	var fort = Math.floor(level / 2) + 2;
	var ref  = Math.floor(level / 3);
	var will = Math.floor(level / 3);
	
	return [fort, ref, will];
}

exports.createBarbarian = function() {
	return new Barbarian();
}