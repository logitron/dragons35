var viewModel;

$(document).ready(function() {
	viewModel = {
		availableClasses: ko.observableArray(),
		selectedClass: ko.observable(),
		
		availableRaces: ko.observableArray(),
		selectedRace: ko.observableArray(),
		
		availableAlignments: ko.observableArray(),
		selectedAlignment: ko.observable(),
		
		availableDeities: ko.observableArray(),
		selectedDeity: ko.observable()
	}
	
	ko.applyBindings(viewModel);
	
	$.getJSON('/classes', function(data) {
		viewModel.availableClasses(data);
	});
	
	$.getJSON('/races', function(data) {
		viewModel.availableRaces(data);
	});
	
	$.getJSON('/alignments', function(data) {
		viewModel.availableAlignments(data);
	});
	
	$.getJSON('/deities', function(data) {
		viewModel.availableDeities(data);
	});
	
	viewModel.selectedRace.subscribe(function(newRace) {
		var size = (typeof newRace != 'undefined') ? newRace.size : '';
		$('#txtSize').html(size);
		
		SetAgeSpecifications();
	});
	
	viewModel.selectedClass.subscribe(function(newClass) {
		SetAgeSpecifications();
	});
});

function SetAgeSpecifications() {
	if (typeof viewModel.selectedRace() == 'undefined' || typeof viewModel.selectedClass() == 'undefined') {
		$('#numAge').attr('disabled', 'disabled');
		$('#numAge').val(0);
		$('#ageExample').html('');
	}
	else {
		var minAge = viewModel.selectedClass().minimumAge[viewModel.selectedRace().name.toLowerCase()];
		
		$('#numAge').removeAttr('disabled');
		$('#numAge').attr('min', minAge);
		$('#ageExample').html('(Minimum Age: ' + minAge + ')');
	}
}