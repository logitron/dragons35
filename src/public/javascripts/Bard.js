// Creating DAO for Barbarian Class
var Bard = function() {
	this.spellsPerDay = [
		[2, 0, 0, 0, 0, 0, 0],
		[3, 0, 0, 0, 0, 0, 0],
		[3, 1, 0, 0, 0, 0, 0],
		[3, 2, 0, 0, 0, 0, 0],
		[3, 3, 1, 0, 0, 0, 0],
		[3, 3, 2, 0, 0, 0, 0],
		[3, 3, 2, 0, 0, 0, 0],
		[3, 3, 3, 1, 0, 0, 0],
		[3, 3, 3, 2, 0, 0, 0],
		[3, 3, 3, 2, 0, 0, 0],
		[3, 3, 3, 3, 1, 0, 0],
		[3, 3, 3, 3, 2, 0, 0],
		[3, 3, 3, 3, 2, 0, 0],
		[4, 3, 3, 3, 3, 1, 0],
		[4, 4, 3, 3, 3, 2, 0],
		[4, 4, 4, 3, 3, 2, 0],
		[4, 4, 4, 4, 3, 3, 1],
		[4, 4, 4, 4, 4, 3, 2],
		[4, 4, 4, 4, 4, 4, 3],
		[4, 4, 4, 4, 4, 4, 4]
	];
}

Bard.prototype.getFeatures = function() {
	
}

Bard.prototype.getAttackBonus = function(level) {
	if (level == 0) return [0];
	
	var result = [];
	
	var attacks = 1 + Math.floor((level - 1) / 7);
	var offset = Math.ceil(level / 4);
	
	for (var i = 0; i < attacks; i++) {
		result.push(level - offset - (i * 5));
	}
	
	return result;
}


Bard.prototype.getSaves = function(level) {
	var fort = Math.floor(level / 3);
	var ref  = Math.floor(level / 2) + 2;
	var will = Math.floor(level / 2) + 2;
	
	return [fort, ref, will];
}

Bard.prototype.getSpellsPerDay = function(level, spellLevel) {
	return this.spellsPerDay[level-1][spellLevel];
}

exports.createBard = function() {
	return new Bard();
}