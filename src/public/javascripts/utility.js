function getAbilityModifier(score) {
	var minModifier = -5;
	
	if (score % 2 != 0) score--;
	
	return minModifier + (score / 2);
}

function getBonusSpells(score, spellLevel) {
	if (spellLevel == 0) return 0;
	
	var offset = (spellLevel * 2) + 2;
	
	return Math.floor((score - offset) / 8);
}

function randomFromTo(from, to){
	return Math.floor(Math.random() * (to - from + 1) + from);
}

function rollDice(maxNumber) {
	return Math.floor(Math.random() * (maxNumber - 1 + 1) + 1);
}

function areScoresLow(scores) {
	console.log(scores);
	
	var highScore = Math.max.apply(Math, scores);
	if (highScore <= 13) return true;
	
	var modTotal = 0;
	for (var i = 0; i < scores.length; i++) {
		modTotal += getAbilityModifier(scores[i]);
	}
	
	return (modTotal <= 0);
}

function getAbilityScores() {
	var scores = [];
	
	for (var i = 0; i < 6; i++) {
		var score = 0;
		var rolls = [];
		
		for (var j = 0; j < 4; j++) {
			var roll = rollDice(6);
			rolls.push(roll);
			score += roll;
		}
		
		var lowScore = Math.min.apply(Math, rolls);
		score -= lowScore;
		scores.push(score);
	}
	
	return scores;
}